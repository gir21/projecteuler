package com.georgeracu.project.euler;

/**
 * Created by George Racu on 14/05/2016.
 */
public class Main {
    public static void main(String[] args){

        runProblem_1();

        runProblem_2();

        runProblem_3();

        runProblem_4();

        runProblem_5();
    }

    /**
     * Problem 1: The sum of all the multiples of 3 or 5 below 1000
     */
    private static void runProblem_1(){
        MultiplesOfThreeAndFive.run();
    }

    /**
     * Problem 2: Even Fibonacci numbers
     */
    private static void runProblem_2(){
        EvenFibonacci.run();
    }

    /**
     * Problem 3: Largest prime factor of the number 600851475143
     */
    private static void runProblem_3(){
        LargestPrimeFactor.run();
    }

    /**
     * Problem 4: Largest palindrome made from the product of two 3-digit numbers
     */
    private static void runProblem_4(){
        LargestPalindromeProduct.run();
    }

    /**
     * Problem 5: the smallest positive number that is evenly divisible
     * by all of the numbers from 1 to 20
     */
    private static void runProblem_5() { SmallestMultiple.run(); }
}
