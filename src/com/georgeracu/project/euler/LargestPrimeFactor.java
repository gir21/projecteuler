package com.georgeracu.project.euler;
import java.util.ArrayList;

/**
 * Created by George Racu on 14/05/2016.
 * The prime factors of 13195 are 5, 7, 13 and 29.
 *
 * What is the largest prime factor of the number 600851475143 ?
 */
class LargestPrimeFactor {
    static void run(){
        // formula for prime numbers larger than 3: 6k-1; 6k+1; k in N;
        ArrayList<Long> listOfPrimes = new ArrayList<>();
        Long largestFactor = 0L;

        // add first prime number to our array
        listOfPrimes.add(3L);
        // calculate first 100000 prime numbers
        int limit = 100000;
        for(int i = 1; i < limit; i++){
            // populate the array list with prime numbers
            listOfPrimes.add(6L * i - 1);
            listOfPrimes.add(6L * i + 1);
        }

        Long numberOfInterest = 600851475143L;
        // for each prime number in our list try to see if is a factor of the number of interest
        for(long prime:listOfPrimes){
            if(numberOfInterest % prime == 0){
                largestFactor = prime;
                numberOfInterest = numberOfInterest/prime;
            }
        }
        System.out.println("The largest prime factor of the number 600851475143 is " + largestFactor);
    }
}
