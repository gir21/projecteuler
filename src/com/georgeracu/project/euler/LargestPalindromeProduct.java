package com.georgeracu.project.euler;

/**
 * Created by George Racu on 14/05/2016.
 *
 * A palindromic number reads the same both ways.
 * The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
 *
 * Find the largest palindrome made from the product of two 3-digit numbers
 */
class LargestPalindromeProduct {
    static void run(){
        System.out.println("The largest palindrome made from the product of two 3-digit numbers is " + getPalindrome());
    }

    private static int getPalindrome() {
        int prod;
        int a = 999;
        int palindrome = 0;
        // start from top limit and stop at the lowest 3 digit number
        while(a > 99){
            int b = a;
            while(b > 99){
                prod = a * b;
                if(isPalindrome(prod) & palindrome < prod){
                    // store the largest palindrome so far
                    palindrome = prod;
                }
                b--;
            }
            a--;
        }
        return palindrome;
    }

    private static boolean isPalindrome(Integer prod){
        String digits = Integer.toString(prod);
        String mirror = "";
        // exclude all odd length arrays
        if(digits.length() % 2 == 0){
            mirror = new StringBuilder(digits).reverse().toString();
        }
        return digits.equals(mirror);
    }
}
