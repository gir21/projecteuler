package com.georgeracu.project.euler;

/**
 * Created by George Racu on 14/05/2016.
 *
 * Description: If we list all the natural numbers below 10 that are multiples of 3 or 5,
 * we get 3, 5, 6 and 9. The sum of these multiples is 23.
 *
 * Find the sum of all the multiples of 3 or 5 below 1000.
 */
class MultiplesOfThreeAndFive {
    static void run(){
        int sum = 0;
        int limit = 1000;
        for(int i = 1; i < limit; i++){
            if(isMultipleOfThreeOrFive(i)){
                sum += i;
            }
        }

        System.out.println("The sum of all the multiples of 3 or 5 below " + limit + " is " + sum);
    }

    private static boolean isMultipleOfThreeOrFive(int val){
        return ((val % 3 == 0) || (val % 5 == 0));
    }
}
